# coding=utf-8

from __future__ import division
from __future__ import print_function

import os
import json

import pygame

DEFAULT_ENCODING = "Windows-1252"

class BitmapFont(object):
    def __init__(self, font_name):
        """Load a bitmap font from a json font definition.

        Example file:
        {
          "filename": "kronbits.png",
          "character_order": "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,;:?!-_~#\"'&()[]|`\\/@°+=*$£€<>%áéíóú ",
          "tile_size": [9, 11],
          "tile_padding": [0, 0],
          "tile_offset": [0, 1],
          "columns": 8,

          "letter_widths": {
            "1": "Iil.:!'| ",
            "2": "JLjr1,;()[]`í",
            "3": "ABCDEFGHKNOPQRSTUVXYZabcdefghknopqstuvxyz023456789?-_\"°=$£€<>áéóú",
            "4": "~&\\/",
            "5": "MWmw#+*",
            "7": "@%"
          }
        }

        :type font_name: str
        """

        # our attributes:
        self.font_height = 0
        self.chr_rect = []
        self.chr_width = []
        self.font_name = font_name
        self.surface = None

        # load the json
        json_filename = os.path.join("fonts", font_name + ".json")
        font_def = json.load(open(json_filename, "rb"))

        # parse the json tree
        surface_filename = os.path.join("fonts", font_def["filename"])
        character_order = font_def["character_order"].encode(DEFAULT_ENCODING)
        tile_size = font_def["tile_size"]
        tile_padding = font_def.get("tile_padding", [0, 0])
        tile_offset = font_def.get("tile_offset", [0, 0])
        columns = font_def["columns"]
        letter_widths = font_def["letter_widths"]

        if len(character_order) % columns == 0:
            rows = int(len(character_order) / columns) - 1
        else:
            rows = int(len(character_order) / columns)

        self.font_height = tile_size[1] - tile_padding[1]

        # load the font image
        self.surface = pygame.image.load(surface_filename)
        self.surface.convert()

        # source area rects for blitting
        def make_rect(index, width):
            xy = (
                ((index % columns) * tile_size[0]) + tile_offset[0],
                (int(index / columns) * tile_size[1]) + tile_offset[1]
            )
            size = (width, tile_size[1] - tile_padding[1])
            return pygame.Rect(xy, size)

        # record the width of each character in the font sheet
        self.chr_width = [tile_size[0] - tile_padding[0]] * 256
        for key, value in letter_widths.items():
            width = int(key)
            characters = value.encode(DEFAULT_ENCODING)
            for character in characters:
                self.chr_width[ord(character)] = width

        # if the character is not known, default to the last character on the sheet
        default_rect = make_rect(columns * rows - 1, tile_size[0] - tile_padding[0])

        # create a draw area rect for each character in the font image
        self.chr_rect = [default_rect] * 256
        for sheet_index, character in enumerate(character_order):
            index = ord(character)
            self.chr_rect[index] = make_rect(sheet_index, self.chr_width[index])

    def size(self, text):
        """
        Determine the amount of space needed to render text

        :type text: str
        :rtype: tuple[width, height]
        """
        width = sum(self.chr_rect[ord(x)].width for x in text.encode(DEFAULT_ENCODING)) + (len(text) - 1)
        return (width, self.font_height)

    def render(self, text):
        # type : (object, str) -> pygame.Surface
        """
        Draw the supplied text to a new surface
        """
        surface = pygame.surface.Surface(self.size(text))
        surface.convert()
        color_key = self.surface.get_colorkey()
        surface.fill(color_key)
        surface.set_colorkey(color_key)
        self.render_text_to(text, surface, (0, 0))
        return surface

    def render_text_to(self, text, surface, topleft):
        """
        Draw the supplied text to a the supplied surface and coordinates

        :type text:
        :type surface: pygame.surface.Surface
        :type topleft: tuple[int, int]
        """
        text = text.encode(DEFAULT_ENCODING)

        topleft = (round(topleft[0]), round(topleft[1]))
        x = topleft[0]

        for char in text:
            area = self.chr_rect[ord(char)]
            surface.blit(self.surface, topleft, area=area)
            x += area.width + 1
            topleft = (x, topleft[1])



