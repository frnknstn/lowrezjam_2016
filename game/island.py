from __future__ import division
from __future__ import print_function

from defines import *
import sprite_types
import game.fonts

class Island(object):
    LOCATIONS = ("1_middle", "2_left", "2_right", "3_left", "3_middle", "3_right")
    CASTLE_POSITIONS = ((7, 11), (16, 8))

    def __init__(self, location, owner, sprite_group):
        """Ocean island, worth points at the end of the game

        :type location: str
        :type owner: Player
        :type sprite_group: pygame.sprite.LayeredUpdates
        """
        self.location = None
        self.owner = owner
        self.sealed = False
        self.wealthy = False

        self.island_sprite = sprite_types.Mob("island", ISLAND_POS[location])
        self.castles = [CastleMob("home", self),
                        CastleMob("away", self)]

        self.move_to_location(location)

        sprite_group.add(self.island_sprite, layer=LAYER_ISLAND)
        sprite_group.add(self.castles[0], layer=LAYER_CASTLE)
        sprite_group.add(self.castles[1], layer=LAYER_CASTLE)
        self.sprite_group = sprite_group

    def move_to_location(self, location):
        """Start an animation to move this island to a location"""
        assert(location in ISLAND_LOCATIONS)

        if location == self.location:
            # no change
            return

        if self.location is None:
            # TODO: play our spawn animation
            self.island_sprite.move_to(*ISLAND_POS[location])
        else:
            # TODO: lerp to the new site
            self.island_sprite.start_anim(
                self.island_sprite.anim_lerp_to(
                    ISLAND_POS[location],
                    0.15, "idle",
                    next_anim=self.island_sprite.anim_idle()
                )
            )

        self.location = location

    def get_friendly_castle(self, player):
        """Return the castle that is on the player's side of the island"""
        if player.side == "home":
            return self.castles[1]
        else:
            return self.castles[0]

    def get_enemy_castle(self, player):
        """Return the castle that is not on the player's side of the island"""
        if player.side == "home":
            return self.castles[0]
        else:
            return self.castles[1]

    def seal(self):
        self.sealed = True
        for castle in self.castles:
            castle.show_score()

    def apply_wealth(self):
        self.wealthy = True

    def die(self):
        """Terminate this island"""
        print("Island dying")
        self.island_sprite.start_anim(
            self.island_sprite.anim_lerp_to(
                (self.island_sprite.pos[0], self.island_sprite.pos[1] + 3),
                0.12, "idle",
                next_anim=None
            )
        )

        for castle in self.castles:
            castle.die()

        # assist in breaking circular loops
        self.castles = []
        self.score_sprites = []


class ScoreSprite(sprite_types.RawSprite):
    def __init__(self, castle, offset):
        """

        :type image: pygame.Surface
        :type castle: CastleMob
        """

        self.castle = castle
        self.offset = offset

        pos = (castle.pos[0] + offset[0], castle.pos[1] + offset[1])
        image = game.fonts.pix_font.render(str(castle.score))

        super(ScoreSprite, self).__init__(image, pos)

        self.castle.island.sprite_group.add(self, layer=LAYER_SPELLS)

    def update(self, dt):
        # follow our island
        x, y = self.castle.pos
        self.move_to(x + self.offset[0], y + self.offset[1])

    def move_to(self, x, y):
        """Move us to the specified coordinates"""
        self.pos = (x, y)
        self.rect.center = (round(x), round(y))


class IslandChildMob(sprite_types.Mob):
    def __init__(self, sprite_name, island, direction="none", offset=(0, 0)):
        """
        Mob that follows its island

        :type sprite_name: str
        :type island_sprite: sprite_types.Mob
        :type offset: tuple[int, int]
        """

        self.offset = offset
        pos = island.island_sprite.pos
        pos = (pos[0] + offset[0], pos[1] + offset[1])

        super(IslandChildMob, self).__init__(sprite_name, pos, direction)

        self.island = island
        self.island_sprite = island.island_sprite


    def update_state(self, dt):
        # follow our island
        pos = self.island.island_sprite.pos
        pos = (pos[0] + self.offset[0], pos[1] + self.offset[1])
        self.move_to(*pos)

    def die(self):
        self.kill()


class CastleMob(IslandChildMob):
    def __init__(self, side, island):
        """
        :type side: "home" or "away"
        :type island_sprite: Island
        """
        if side == "home":
            sprite_name = "castle1"
        elif side == "away":
            sprite_name = "castle2"
        else:
            raise NotImplementedError("side '%s'" % side)

        super(CastleMob, self).__init__(sprite_name, island)

        self.damage = 0
        self.side = side
        self.aura = None
        self.aura_sprite = None
        self.shielded = False
        self.shield_sprite = None
        self.island_sprite = island.island_sprite
        self.score_sprite = None

        self.start_anim(self.anim_once("build", 0.10, next_anim=self.anim_stand))

    def set_aura(self, spell_type):
        """
        Assign an aura type to this castle, preventing other types of magic

        :type spell_type: str or None
        """
        if spell_type is None and self.aura is not None:
            # remove our aura
            print("removing our aura")
            self.aura = None
            if self.aura_sprite:
                self.aura_sprite.die()
        else:
            # start the aura change
            print("setting aura to %s" % spell_type)
            assert spell_type in AURAS
            self.aura = spell_type

            if self.aura_sprite:
                self.aura_sprite.die()

            # work out the correct sprite to use for our aura
            if self.side == "home":
                direction = "left"
            else:
                direction = "right"

            if spell_type == "fire":
                sprite_name = "fireaura"
            elif spell_type == "ice":
                sprite_name = "iceaura"
            else:
                sprite_name = "zapaura"

            self.aura_sprite = IslandChildMob(sprite_name, self.island, direction)
            self.island.sprite_group.add(self.aura_sprite, layer=LAYER_ISLAND_DETAIL)

    def anim_stand(self):
        """Chose a frame based on current damage"""
        frames = self.get_frames("stand")

        while True:
            frame_index = max(0, len(frames) - self.damage - 1)
            new_frame = frames[frame_index]
            if new_frame == self.image:
                yield False
            else:
                debug("%s %s now shows %d damage" % (self.sprite_name, str(self), self.damage))
                self.image = new_frame
                yield True

    def show_score(self):
        """Start showing our score sprite"""
        if self.side == "home":
            offset = CASTLE_OFFSET_HOME
        else:
            offset = CASTLE_OFFSET_AWAY
        self.score_sprite = ScoreSprite(self, offset)

    @property
    def score(self):
        """The current score value of this castle"""
        score = CASTLE_MAX_DAMAGE - self.damage
        return score

    def add_shield(self):
        """enable the shield"""
        self.shielded = True

        # work out the correct sprite to use for our sprite
        if self.side == "home":
            direction = "left"
            offset = SHIELD_OFFSET_HOME
        else:
            direction = "right"
            offset = SHIELD_OFFSET_AWAY

        print("Shield direction %s" % direction )

        if self.aura == "fire":
            sprite_name = "fireshield"
        elif self.aura == "ice":
            sprite_name = "iceshield"
        else:
            sprite_name = "zapshield"

        self.shield_sprite = IslandChildMob(sprite_name, self.island, direction, offset=offset)
        self.island.sprite_group.add(self.shield_sprite, layer=LAYER_CASTLE_DETAIL)

    def die(self):
        """Terminate this mob"""
        # trigger our death animations
        print("Castle dying")
        self.start_anim(self.anim_once("build", 0.025, next_anim=None, reverse_order=True))

        if self.aura_sprite:
            self.aura_sprite.die()
        if self.score_sprite:
            self.score_sprite.die()
        if self.shield_sprite:
            self.shield_sprite.die()

