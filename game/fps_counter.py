from __future__ import division
from __future__ import print_function

import sys
import time

from defines import pygame_default_font


class FPSCounter(object):
    frame_buffer_size = 60

    def __init__(self, font=None):
        if font is None:
            try:
                font = pygame_default_font(16)
            except IOError:
                # unable to read default font file
                print("Warning: Unable to load default font for FPS display")

        # chose the best time function
        if sys.platform.startswith('linux'):
            self.time_func = time.time
        else:
            self.time_func = time.clock

        # class variables
        self.last_frame = self.time_func()
        self.frame_count = 0
        self.average_frame_time = 0
        self.font = font

        # circular buffer
        self.frame_time_buffer = [0.0] * self.frame_buffer_size
        self.frame_time_index = 0


    def frame(self):
        """Record the current time as the end of the frame"""
        current_time = self.time_func()
        delta = current_time - self.last_frame

        # populate the circular frame time buffer
        frame_time_buffer = self.frame_time_buffer
        self.frame_time_index += 1
        if self.frame_time_index >= self.frame_buffer_size:
            self.frame_time_index = 0
        frame_time_buffer[self.frame_time_index] = delta

        self.last_frame = current_time
        self.frame_count += 1
        self.average_frame_time = sum(self.frame_time_buffer) / self.frame_buffer_size

        return 1 / self.average_frame_time

    def draw(self, surf):
        if self.font:
            surf.blit(self.font.render("FPS: %0.3f" % (1 / self.average_frame_time), True, (0xFF, 0xFF, 0xFF), (0,0,0)), (0, 0))
