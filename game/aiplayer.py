from __future__ import division
from __future__ import print_function

import random
from defines import ACTION_TYPES

import game

class AIController(object):
    def __init__(self, wizard):
        # type: ("game.player.Player") -> AIController
        """Super-dumb random player events source."""
        self.wizard = wizard
        
        self.timeout = game.get_time() + 2
    
    def generate_actions(self):
        # type: ()->None
        """populate our wizard with actions"""
        
        wizard = self.wizard
        curr_time = game.get_time()
        
        if curr_time < self.timeout:
            # too soon
            return
        
        if not wizard.active_spell:
            # get a new spell
            if len(wizard.storage) == 0:
                action = "draw from aether"
            else:
                action = random.choice(("draw from aether", "draw from aether", "draw from staff"))
        else:
            action = random.choice(
                (
                    "cast",
                    "cast",
                    "cast",
                    "cast",
                    "cast",
                    "cast reversed",
                    "draw from aether",
                    "draw from aether",
                    "draw from staff"
                )
            )
        
        target = None
        if action in ("cast", "cast reversed"):
            target = random.choice((0, 0, 1, 1, 1, 2, 2))
        
        # queue our action
        print("AI Action: wizard %s does '%s' at '%s'" % (wizard.name, action, str(target)))
        wizard.add_action(wizard, action, target)
        
        # delay before next action
        self.timeout = curr_time + random.uniform(1.1, 3)
