#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import pygame.font as pygame_font_module      # avoid polluting other module's namespaces

DEBUG = False
DEBUG = True

BENCHMARK = False
BENCHMARK = True

RESOURCE_CATEGORIES = ("tiles", "interface", "icons", "music", "sound")
SPRITE_DIR = "sprites"
CAP_FPS = None
CAP_FPS = 60
CAP_FPS = 90
CAP_FPS = 300
#CAP_FPS = 999

DOUBLE_SIZE = False
DOUBLE_SIZE = True

SCREEN_SIZE = (64, 64)
REAL_SCREEN_SIZE = (64 * 8, 64 * 8)


DIRECTIONS = ("none", "up", "right", "down", "left")

# game constants
ACTION_TYPES = (
    "cast",
    "cast reversed",
    "draw from aether",
    "draw from staff"
)

SPELL_COLORS = (
    "black",
    "blue",
    "red",
    "pink"
)

SPELLS = {
    "fire": "red",
    "ice": "blue",
    "zap": "pink",
    "wave": "black",
    "island": "black",
    "f shield": "red",
    "i shield": "blue",
    "z shield": "pink",
    "dispel": "black",
    "wealth": "black",
    "seal": "black"
    # "fire2x",
    # "ice2x",
    # "zap2x"
    # "rotate",
    # "meteor",
    # "leak",
    # "volcano",
    # "earthquake"
}

AURAS = (
    "fire",
    "ice",
    "zap"
)

ISLAND_LOCATIONS = (
    "1_middle",
    "2_left",
    "2_right",
    "3_left",
    "3_middle",
    "3_right"

)

# layout
WIZARD_HOME_POS = (10, 46)
WIZARD_AWAY_POS = (58, 17)

WIZARD_ACTIVE_AETHER_POS = (WIZARD_HOME_POS[0] - 4, WIZARD_HOME_POS[1] - 14)
WIZARD_STORED_AETHER_POS = (WIZARD_HOME_POS[0] + 2, WIZARD_HOME_POS[1] - 7)

ISLAND_POS = {
    "1_middle": (30, 37),
    "2_left": (26, 32),
    "2_right": (36, 46),
    "3_left":(19, 23),
    "3_middle": (31, 35),
    "3_right": (40, 49)
}

CASTLE_OFFSET_HOME = (-5, -4)
CASTLE_OFFSET_AWAY = (5, -7)

SHIELD_OFFSET_HOME = (-5, -2)
SHIELD_OFFSET_AWAY = (5, -4)

CLOUD_POS_HOME = (7, 7)

LAYER_BACKGROUND = -2
LAYER_BACKGROUND_DETAIL = -1
LAYER_ISLAND = 0
LAYER_ISLAND_DETAIL = 1
LAYER_WIZARD = 2
LAYER_CASTLE = 3
LAYER_CASTLE_DETAIL = 4
LAYER_SPELLS = 5

CASTLE_MAX_DAMAGE = 5

# functions
def debug(*args):
    if DEBUG:
        print(*args)

def pygame_default_font(size):
    """
    Emulate the output of pygame.font.SysFont("", size), but in a way that doesn't crash if run through cx_Freeze

    :type size: int
    :rtype: pygame.font.Font
    """
    
    # reduce the size to match what pygame's font.c:font_init() does
    # reportedly, this is to keep the modern pygame default font roughly the same size as that of older pygame versions.
    size = int(size * 0.6875)
    if size < 1:
        size = 1
    
    return pygame_font_module.Font("fonts/freesansbold.ttf", size)
