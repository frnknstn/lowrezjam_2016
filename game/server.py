from __future__ import division
from __future__ import print_function

from threading import Thread, Event
from Queue import Empty as QueueEmpty

class DummyServer(Thread):
    def __init__(self, client_send_queue, client_recv_queue, *args, **kwargs):
        """A proxy object for the server thread.
        
        The Server object is very simple. It serialises the actions of both players, 
        providing a "hot-seat equivalent" system.
        
        Any clients must take care to not tie internal state to the effect of an action.
        For example, you can't have an action work differently based on what frame of 
        animation a mob is on.
        """
        Thread.__init__(self, name="server-thread", *args, **kwargs)

        self.client_send_queue = client_send_queue
        self.client_recv_queue = client_recv_queue

        self.stop_event = Event()

    def run(self):
        """Act as a dummy echo server"""
        print("*** server: starting")

        while not self.stop_event.is_set():
            try:
                while True:
                    action = self.client_send_queue.get(block=True, timeout=0.1)
                    print("*** server: forwarding action", action)
                    self.client_recv_queue.put(action)
            except QueueEmpty:
                pass

        print("*** server: ending")

    def stop(self):
        self.stop_event.set()
        print("*** server: setting stop event")

