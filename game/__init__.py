# coding=utf-8

from __future__ import division
from __future__ import print_function

import itertools
import os
import sys
import random
import time
from six.moves.queue import Queue, Empty as QueueEmpty
from types import FunctionType, LambdaType

import pygame

from fps_counter import FPSCounter
import resource

from defines import *

# import interface
import resource
import sprite_types
from game.player import Player, AetherCloud
from sprite_types import Sprite
from island import Island
from game.server import DummyServer as Server
from game.menus import MenuItem, Menu
from game.aiplayer import AIController
import game.fonts

# util functions
# chose the best time function
if sys.platform.startswith('linux'):
    get_time = time.time
else:
    get_time = time.clock

# classes
class Game(object):
    GAME_STATES = {"start", "pre-game", "intro", "main", "post-level", "scores"}

    def __init__(self, screen_size, real_screen_size=None, name="pygame game", cap_fps=CAP_FPS):
        self.state = "start"
        self.running = True

        self.screen = None  # type: pygame.Surface
        self._real_screen = None

        self.cap_fps = cap_fps
        self.clock = pygame.time.Clock()
        self.current_time = 0

        self.window_caption = name

        # create our window
        self.set_screen_size(screen_size, real_screen_size)
        pygame.display.set_caption(self.window_caption)

        self.set_state("start")

    def set_screen_size(self, screen_size, real_screen_size=None):
        """
        Set our window resolution, and optionally our scaling

        :type screen_size: tuple[int, int]
        :type real_screen_size:  tuple[int, int] or None
        """
        if real_screen_size is None:
            # No upscaling
            self.screen = pygame.display.set_mode(screen_size)
        else:
            self._real_screen = pygame.display.set_mode(real_screen_size)
            self.screen = pygame.Surface(screen_size)
            self.screen.convert(self._real_screen)

    def flip(self, surface=None):
        """Move the screen surface to the actual display"""
        if surface is None:
            surface = self.screen

        if self._real_screen is not None:
            pygame.transform.scale(surface, self._real_screen.get_size(), self._real_screen)

        pygame.display.flip()

        if CAP_FPS is not None:
            dt = self.clock.tick(CAP_FPS) / 1000
        else:
            # be a good multitasking program buddy
            time.sleep(0.0005)
            dt = self.clock.tick() / 1000

        self.current_time += dt
        return dt

    def set_state(self, state):
        assert(state in self.GAME_STATES)
        debug("Game state changed to '%s'" % state)
        self.state = state

    def parse_events(self, handlers=None):
        """Parse the PyGame event queue, handling global keys, or passing them off to a handler otherwise.

        You can supply multiple handler
        """

        def dummy_handler(event):
            """dummy event handler"""
            return False

        def try_handlers(event):
            """try all our handlers in order, returns True if any of them handled it"""
            handled = False
            for handler in handlers:
                handled = handler(event)
                if handled:
                    return True
            return False

        if handlers is None:
            handlers = (dummy_handler,)
        elif callable(handlers):
            handlers = (handlers,)

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if not try_handlers(event):
                    if event.key in (pygame.K_ESCAPE, ):
                        # default handler just exits on Esc
                        self.running = False
                    else:
                        # unhandled event
                        debug(event)

            elif event.type == pygame.KEYUP:
                try_handlers(event)

            elif event.type == pygame.QUIT:
                self.running = False

    def flash_screen(self, next_state, duration=0.5):
        """Draw a whole-screen flash transition"""
        screen = self.screen

        flash_surface = pygame.Surface(screen.get_size())
        flash_surface.fill((255, 255, 255))

        back_buffer = pygame.Surface(screen.get_size())
        back_buffer.blit(screen, (0, 0))

        frames = 0
        duration = 0.50
        step_duration = duration / 255
        start_time = get_time()

        while self.running:
            self.parse_events()

            screen.blit(back_buffer, (0, 0))

            fade_time = get_time() - start_time
            alpha = min(int(fade_time / step_duration), 255)
            flash_surface.set_alpha(alpha)
            screen.blit(flash_surface, (0, 0))

            self.flip()
            frames += 1

            if fade_time > duration:
                break

        debug("flash_screen() took %d frames for a %f second fade" % (frames, duration))
        self.set_state(next_state)

    def main(self):
        """Example for game main entry point. This should be overridden by any child classes"""
        debug("Game.main() running")

        while self.running:
            if self.state == "start":
                self.flash_screen("main", 0.05)
            elif self.state == "main":
                self.main_game()
            elif self.state == "post-level":
                # post-game interlude
                self.flash_screen("scores")
            elif self.state == "scores":
                self.score_screen()
                self.running = False

        debug("Leaving main loop")

    def main_game(self):
        """An example of the main game loop. This should be overridden by child classes"""
        fps = FPSCounter()

        # prepare the game

        # Main Loop
        self.flip()
        while self.running:
            # parse events and input
            self.parse_events()

            # move the sprites
            # draw the frame

            # complete the frame
            average_fps = fps.frame()

            if DEBUG:
                # draw debug info
                pygame.display.set_caption("%s - %d FPS - %0.3f sec" % (self.window_caption, average_fps, self.current_time))

            self.flip()

            # check if our state changed during this frame
            if self.state != "main":
                debug("breaking to state %s" % self.state)
                break


class BlastGame(Game):
    GAME_STATES = {"start", "menu", "tutorial", "main", "scores"}

    def __init__(self, *args, **kwargs):
        super(BlastGame, self).__init__(*args, **kwargs)

        self.islands = []   # type: list[Island]
        self.players = []   # type: list[Player]

        self.send_queue = Queue()
        self.recv_queue = Queue()

        self.server = None  # type: Server

    def main(self):
        debug("Game.main() running")

        # prepare the stuff
        resource.load_resources()
        sprite_types.load_sprite_files()
        game.fonts.init()

        score = (0, 0)

        while self.running:
            if self.state == "start":
                self.flash_screen("menu")
            elif self.state == "menu":
                self.menu_screen()
            elif self.state == "main":
                score = self.main_game()
            elif self.state == "scores":
                self.score_screen(score)
            elif self.state == "tutorial":
                self.tutorial_game()
            else:
                raise "ERROR: Unhandled game state: %s" % str(self.state)

        debug("Leaving main loop")

    def main_game(self):
        """The main game"""
        print("Creating a new game =============")
        
        screen = self.screen
        fps = FPSCounter()

        pix_font = game.fonts.pix_font
        pix_font_grey = game.fonts.pix_font_grey

        render_group = pygame.sprite.LayeredUpdates()

        background = Sprite("map", screen.get_rect().center)
        waves = sprite_types.WaveSprite("waves", (screen.get_rect().center[0], 7.5),
                                        (screen.get_rect().width/4, 0), 20)

        wizard1 = Player("homeplayer", "home", render_group, local=True)
        wizard2 = Player("homeplayer", "away", render_group)
        self.players = [wizard1, wizard2]

        self.islands = [
            island.Island("3_middle", wizard1, render_group)
        ]

        self.server = Server(self.send_queue, self.recv_queue)
        self.server.start()

        wizard1.add_cloud_to_render_group()

        render_group.add(background, layer=LAYER_BACKGROUND)
        render_group.add(waves, layer=LAYER_BACKGROUND_DETAIL)
        
        ai = AIController(wizard2)

        # populate the level

        # prepare the GUI
        spell_text_surface = None

        render_group.draw(screen)
        dt = self.flip()

        # Main Loop
        while self.running:
            # parse events and input
            self.parse_events(handlers=(wizard1.handle_event, wizard2.handle_event_away))
            ai.generate_actions()

            self.send_player_actions(wizard1)
            self.send_player_actions(wizard2)

            actions = self.recv_actions()

            self.handle_actions(actions)

            # draw interfaces
            # for item in interface.interface_list:
            #     item.draw()

            render_group.update(dt)
            render_group.draw(screen)

            # draw spell names to screen
            def render_spell_name(font, spell, surf, pos, align_left=True):
                if spell:
                    spell_type = spell.spell_type
                    spell_color = spell.color
                else:
                    spell_type = ""
                    spell_color = "black"

                x_offset = 4
                if spell_color == "red":
                    icon = resource.res("icons", "fire_symbol_narrow")
                elif spell_color == "blue":
                    icon = resource.res("icons", "ice_symbol_narrow")
                elif spell_color == "pink":
                    icon = resource.res("icons", "zap_symbol_narrow")
                else:
                    x_offset = 0
                    icon = None

                if align_left:
                    # align our left edge to the supplied pos
                    icon_pos = (pos[0], pos[1] + 3)
                    text_pos = (pos[0] + x_offset, pos[1])
                else:
                    # align our right edge to the supplied pos
                    icon_pos = (pos[0] - x_offset, pos[1] + 3)
                    text_pos = (pos[0] - font.size(spell_type)[0] - x_offset - 1, pos[1])

                if icon:
                    surf.blit(icon, icon_pos)
                font.render_text_to(spell_type, screen, text_pos)

            #pix_font.render_text_to(getattr(wizard1.active_spell, "spell_type", ""), screen, (1, 53))
            render_spell_name(pix_font, wizard1.active_spell, screen, (1, 53))
            if wizard1.storage:
                font_left = SCREEN_SIZE[0] - 1
                render_spell_name(pix_font, wizard1.storage[-1], screen, (font_left, 53), False)

            # complete the frame
            average_fps = fps.frame()

            if DEBUG:
                # draw debug info
                pygame.display.set_caption("%s - %d FPS  - %0.3f sec - %d frames - %d sprites" %
                    (self.window_caption, average_fps, self.current_time, fps.frame_count, len(render_group)))

            dt = self.flip()

            # check for victory
            all_islands_sealed = True
            for item in self.islands:
                if not item.sealed:
                    all_islands_sealed = False
                    break
            if all_islands_sealed:
                # trigger the end of the game
                self.set_state("scores")

            if self.state != "main":
                # leave the main loop
                debug("breaking to state %s" % self.state)
                break

        return self.calculate_scores()

    def send_player_actions(self, player):
        """Send a player's actions to the server"""
        for action in player.actions:
            self.send_queue.put(action)

        # clear the actions list in-place
        player.actions *= 0

    def recv_actions(self):
        """Read actions from the servers

        :rtype: list[Action]
        """
        retval = []
        try:
            while True:
                retval.append(self.recv_queue.get_nowait())
        except QueueEmpty:
            pass

        return retval

    def handle_actions(self, actions):
        """process all of the player's actions that were generated from their input

        :type actions: list[player.Action]
        """
        home_player, away_player = None, None

        # work out which player is which
        for item in self.players:
            if item.side == "home":
                home_player = item
            else:
                away_player = item

        # do the actions
        for action in actions:
            if action.player_side == "home":
                player = home_player
            else:
                player = away_player

            if action.action_type in ("cast", "cast reversed"):
                cast_success = False
                active_spell = player.active_spell
                if not active_spell:
                    print("Unable to cast spell with no active spell")
                    continue
                assert isinstance(active_spell, game.player.Spell)

                # damage spells
                if active_spell.spell_type in ("fire", "ice", "zap"):
                    island = self.get_island_at_index(action.target)
                    if island is None:
                        print("target does not correspond to an island")
                    elif island.sealed:
                        print("can't target a sealed island")
                    else:
                        # we have a damage spell and a target island
                        if action.action_type == "cast":
                            castle = island.get_friendly_castle(player)
                        else:  # action.action_type == "cast_reversed":
                            castle = island.get_enemy_castle(player)

                        # check if this is a valid damage spell cast action
                        if castle.aura is None:
                            castle.set_aura(active_spell.spell_type)

                        if castle.aura == active_spell.spell_type:
                            if not castle.shielded:
                                # do the damage
                                castle.damage += 1
                                cast_success = True
                            else:
                                print("can't damage a shielded castle")
                        else:
                            print("Unable target castle with %s due to %s aura" % (
                                active_spell.spell_type, str(castle.aura))
                            )

                # shields
                elif active_spell.spell_type in ("f shield", "i shield", "z shield"):
                    island = self.get_island_at_index(action.target)
                    if island is None:
                        print("target does not correspond to an island")
                    elif island.sealed:
                        print("can't target a sealed island")

                    else:
                        # we have a shield spell and a target island, let's assist the friendlies
                        if action.action_type == "cast":
                            castle = island.get_enemy_castle(player)
                        else:  # action.action_type == "cast_reversed":
                            castle = island.get_friendly_castle(player)

                        # work out what the corresponding aura is
                        if active_spell.spell_type == "f shield":
                            shield_aura = "fire"
                        elif active_spell.spell_type == "i shield":
                            shield_aura = "ice"
                        elif active_spell.spell_type == "z shield":
                            shield_aura = "zap"
                        else:
                            raise "Unknown spell type %s" % active_spell.spell_type

                        # check if shield cast is valid
                        if castle.shielded:
                            print("Unable to add shield, already shielded")
                        elif not castle.aura:
                            print("Unable to add shield, no aura")
                        elif castle.aura != shield_aura:
                            print("Unable to add shield, wrong aura")
                        else:
                            print("adding shield to %s" % str(castle))
                            castle.add_shield()
                            cast_success = True

                # island-related spells
                elif active_spell.spell_type == "island":
                    if self.create_island_at_index(action.target, player):
                        cast_success = True
                elif active_spell.spell_type == "wave":
                    if self.clear_island_at_index(action.target):
                        cast_success = True
                elif active_spell.spell_type == "wealth":
                    island = self.get_island_at_index(action.target)
                    if island is None:
                        print("target does not correspond to an island")
                    elif island.sealed:
                        print("can't target a sealed island")
                    else:
                        island.apply_wealth()
                        cast_success = True
                elif active_spell.spell_type == "seal":
                    island = self.get_island_at_index(action.target)
                    if island is None:
                        print("target does not correspond to an island")
                    elif island.sealed:
                        print("can't target a sealed island")
                    else:
                        island.seal()
                        cast_success = True

                else:
                    print("WARNING: Unimplemented spell type '%s'" % active_spell.spell_type)
                    cast_success = True

                # spell cast complete
                if cast_success:
                    player.active_spell.hide()
                    player.active_spell.aether.kill()
                    player.active_spell = None

            elif action.action_type == "draw from aether":
                old_spell = player.active_spell

                new_spell = player.draw_from_aether()

                if new_spell:
                    # hide our current aether
                    if old_spell:
                        aether = old_spell.aether
                        aether.start_anim(aether.anim_lerp_to(
                            WIZARD_STORED_AETHER_POS, 0.1, "idle", next_anim=aether.anim_hide()))

                    aether = new_spell.aether
                    aether.start_anim(aether.anim_lerp_to(
                        WIZARD_ACTIVE_AETHER_POS, 0.2, "idle", next_anim=aether.anim_idle()))

            elif action.action_type == "draw from staff":
                old_spell = player.active_spell

                new_spell = player.draw_from_staff()

                if new_spell:
                    # hide our current aether
                    if old_spell:
                        aether = old_spell.aether
                        aether.start_anim(aether.anim_lerp_to(
                            WIZARD_STORED_AETHER_POS, 0.1, "idle", next_anim=aether.anim_hide()))

                    # show our recovered aether
                    aether = new_spell.aether
                    aether.move_to(*WIZARD_STORED_AETHER_POS)
                    aether.start_anim(aether.anim_lerp_to(
                        WIZARD_ACTIVE_AETHER_POS, 0.1, "idle", next_anim=aether.anim_idle()))

            else:
                raise NotImplementedError("Action type '%s' not yet implemented :(" % action.action_type)

    def get_island_at_index(self, index):
        """Get the island at a board position index (q=0, w=1, e=2)

        :type index: int
        :rtype: Island or None
        """
        if len(self.islands) == 0:
            # no islands
            return None
        elif len(self.islands) == 1:
            # only one island, assume they mean this one
            return self.islands[0]

        # two or three islands
        if index == 0:
            # q position
            return self.islands[0]
        elif index == 1:
            # w position
            if len(self.islands) == 3:
                return self.islands[1]
            else:
                return None
        elif index == 2:
            # e position
            return self.islands[-1]

    def create_island_at_index(self, index, owner):
        """Create a new island if possible

        :type index: int
        :type owner: Player
        :rtype: island
        """
        pos = None
        new_island = None
        
        if len(self.islands) == 0:
            new_island = Island("1_middle", owner, owner.render_group)
            self.islands.append(new_island)
        elif len(self.islands) >= 3:
            print("Warning: refusing to create island after the third")
            return None
        else:
            if index == 0:
                if len(self.islands) == 1:
                    pos = "2_left"
                elif len(self.islands) == 2:
                    pos = "3_left"
                new_island = Island(pos, owner, owner.render_group)
                self.islands.insert(0, new_island)
            elif index == 1:
                print("Warning: refusing to create new island in middle position")
                return None
            elif index == 2:
                if len(self.islands) == 1:
                    pos = "2_right"
                elif len(self.islands) == 2:
                    pos = "3_right"
                new_island = Island(pos, owner, owner.render_group)
                self.islands.append(new_island)

        self.recenter_islands()
        return new_island

    def clear_island_at_index(self, index):
        """Attempt to clear an island

        :type index: int
        :rtype: bool
        """
        # find the targetted island
        if index == 1 or len(self.islands) == 1:
            # can't ever clear the middle island
            print("refusing to clear middle island")
            return False

        island = self.get_island_at_index(index)
        if not island:
            print("Can't find a target island at this index")
            return False

        # clear if possible
        if island.sealed:
            print("refusing to clear a sealed island")
            return False
        else:
            self.islands.remove(island)
            self.recenter_islands()
            island.die()
            return True

    def recenter_islands(self):
        """move all islands to their correct locations based on their position in our list"""
        assert len(self.islands) <= 3
        if len(self.islands) == 1:
            self.islands[0].move_to_location("1_middle")
        elif len(self.islands) == 2:
            self.islands[0].move_to_location("2_left")
            self.islands[1].move_to_location("2_right")
        else:
            self.islands[0].move_to_location("3_left")
            self.islands[1].move_to_location("3_middle")
            self.islands[2].move_to_location("3_right")

    def calculate_scores(self):
        # type: () -> Tuple[int, int]
        """Return a 2-tuple of scores, (home, away)"""
        home = away = 0
        for item in self.islands:
            a, b = [ x.score for x in item.castles ]
            if not item.wealthy:
                value = 1
            else:
                value = 2
            if a > b:
                home += value
            elif b > a:
                away += value

        return (home, away)

    def menu_screen(self):
        """display the main menu"""

        screen = self.screen

        # prepare the menu
        font = game.fonts.pix_font

        menu = Menu((
            MenuItem("-- Blast! Menu --", font, selectable=False),
            MenuItem("Play", font),
            MenuItem("Tutorial", font),
            MenuItem("Exit", font)
            ), 1)

        cursor = font.render(">")

        # menu loop
        dt = self.flip()
        while self.running:
            # draw the menu
            screen.fill((0, 0, 0))
            y = 1
            for item in menu.items:
                if item.selectable:
                    if item.selected:
                        screen.blit(cursor, (2, y))
                    x = cursor.get_width() + 4
                else:
                    x = 1

                screen.blit(item.surf, (x, y))
                y = y + item.height + 1

            self.parse_events(handlers=menu.handle_event)

            # check if anything got selected
            activated_item_text = menu.get_activated()
            if activated_item_text:
                if activated_item_text == "Exit":
                    self.running = False
                elif activated_item_text == "Tutorial":
                    self.set_state("tutorial")
                elif activated_item_text == "Play":
                    self.set_state("main")
                else:
                    print("Unknown menu option: %s" % activated_item_text)

            if self.state != "menu":
                # leave the main loop
                debug("breaking to state %s" % self.state)
                break

            dt = self.flip()

    def score_screen(self, score):
        # type: (Tuple[int, int]) -> None
        """display the game's final score"""
        screen = self.screen
        font = game.fonts.pix_font
        if score[0] > score[1]:
            win_message = "You WIN!"
        elif score[0] < score[1]:
            win_message = "You LOSE!"
        else:
            win_message = "It's a TIE!"

        menu = Menu((
            MenuItem("-- Results --", font, selectable=False),
            MenuItem("Home: %d Away: %d" % score, font, selectable=False),
            MenuItem(win_message, font, selectable=False),
            MenuItem("Replay", font),
            MenuItem("Menu", font),
            MenuItem("Exit", font)
            ), 3)

        cursor = font.render(">")

        background = screen.copy()
        background.set_alpha(128)
        screen.fill((0, 0, 0))
        screen.blit(background, (0, 0))
        background = screen.copy()

        # menu loop
        dt = self.flip()
        while self.running:
            # draw the menu
            screen.blit(background, (0, 0))

            y = 1
            for item in menu.items:
                if item.selectable:
                    if item.selected:
                        screen.blit(cursor, (2, y))
                    x = cursor.get_width() + 4
                else:
                    x = 1

                screen.blit(item.surf, (x, y))
                y = y + item.height + 1

            self.parse_events(handlers=menu.handle_event)

            # check if anything got selected
            activated_item_text = menu.get_activated()
            if activated_item_text:
                if activated_item_text == "Exit":
                    self.running = False
                elif activated_item_text == "Replay":
                    self.set_state("main")
                elif activated_item_text == "Menu":
                    self.set_state("menu")
                else:
                    print("Unknown menu option: %s" % activated_item_text)

            if self.state != "scores":
                # leave the main loop
                debug("breaking to state %s" % self.state)
                break

            dt = self.flip()


    def tutorial_game(self):
        #stub
        self.set_state("main")

    #
    # test functions:
    
    def test_font(self):
        """test fonts and font sizes"""
        screen = self.screen
        fps = FPSCounter()

        # prepare the game
        msg = u"The quick brown fox, jumped over! The LAZY... dog? ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,;:?!-_~#\"'&()[]|`\/@°+=*$£€<>%áéíóú "

        font = game.fonts.pix_font

        # Main Loop
        self.flip()
        while self.running:
            # parse events and input
            self.parse_events()

            screen.fill((0,255,255))

            text_x = 0 - int((self.current_time % 25) * 20)

            # font.render_text_to(msg, screen, (text_x, 0))
            # font.render_text_to(msg, screen, (text_x-10, 10))
            # font.render_text_to(msg, screen, (text_x-20, 20))
            # font.render_text_to(msg, screen, (text_x-30, 30))
            # font.render_text_to(msg, screen, (text_x-40, 40))
            # font.render_text_to(msg, screen, (text_x-50, 50))
            # font.render_text_to(msg, screen, (text_x-60, 60))

            text_surface = font.render(msg)
            screen.blit(text_surface, (text_x, 0))
            screen.blit(text_surface, (text_x-10, 10))
            screen.blit(text_surface, (text_x-20, 20))
            screen.blit(text_surface, (text_x-30, 30))
            screen.blit(text_surface, (text_x-40, 40))
            screen.blit(text_surface, (text_x-50, 50))
            screen.blit(text_surface, (text_x-60, 60))

            # complete the frame
            average_fps = fps.frame()

            if DEBUG:
                # draw debug info
                pygame.display.set_caption("%s - %f FPS - %0.3f sec - %d frames" % 
                    (self.window_caption, average_fps, self.current_time, fps.frame_count))

            self.flip()

            # check if our state changed during this frame
            if self.state != "test-font":
                debug("breaking to state %s" % self.state)


def main(game_class=Game):
    my_game = None
    try:
        pygame.init()

        # resource.load_resources()

        my_game = game_class(SCREEN_SIZE, REAL_SCREEN_SIZE, name="Blast!")
        my_game.main()

    finally:
        debug("Stopping pygame...")
        if my_game and my_game.server:
            my_game.server.stop()
            my_game.server.join()

        pygame.quit()

    debug("Exiting.")
