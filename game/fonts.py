"""Module holding the font definitions shared throughout the project"""

from __future__ import division
from __future__ import print_function

from bitmap_font import BitmapFont

pix_font = None         # type: BitmapFont
pix_font_grey = None    # type: BitmapFont

def init():
    global pix_font
    global pix_font_grey

    pix_font = BitmapFont("kronbits")
    pix_font_grey = BitmapFont("kronbits_grey")
