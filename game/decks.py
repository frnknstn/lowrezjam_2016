"""
This file currently holds the basic decks
"""

from __future__ import division
from __future__ import print_function

basic = \
    ["fire"]    * 9 + \
    ["ice"]     * 5 + \
    ["zap"]     * 5 + \
    ["f shield"]* 2 + \
    ["i shield"]* 2 + \
    ["z shield"]* 1 + \
    ["wave"]    * 2 + \
    ["island"]  * 4 + \
    ["dispel"]  * 1 + \
    ["wealth"]  * 1