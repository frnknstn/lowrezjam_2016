from __future__ import division
from __future__ import print_function

from itertools import cycle

import pygame

class MenuItem(object):
    def __init__(self, text, font, selectable=True):
        """Basic menu item class"""
        self.selected = False
        self.selectable = selectable
        self.activated = False
        self.text = text
        self.surf = font.render(text)

    @property
    def width(self):
        return self.surf.get_width()

    @property
    def height(self):
        return self.surf.get_height()


class Menu(object):
    def __init__(self, menu_items, default_index):
        # type: (Sequence[MenuItem], int) -> object
        """Rudimentary menu helpers

        :type menu_items: sequence of MenuItem
        """
        self.items = menu_items[:]
        self.items[default_index].selected = True

    def activate(self):
        # type: () -> Optional[str]
        """Mark the selected item as activated and return its text"""
        for item in self.items:
            if item.selected:
                item.activated = True
                return item.text
        return None

    def get_activated(self):
        # type: () -> Optional[str]
        """Get the text of the first item is marked as activated, and then clear the activation"""
        for item in self.items:
            if item.activated:
                item.activated = False
                return item.text
        return None

    def get_selected(self):
        # type: () -> Optional[str]
        """Get the text of the first item is marked as selected"""
        for item in self.items:
            if item.selected:
                return item.text
        return None

    def select_next(self, prev=False):
        """select the next valid item, looping"""
        checked_items = set()
        found = False

        if not prev:
            menu_items = self.items
        else:
            menu_items = reversed(self.items)

        for item in cycle(menu_items):
            if not item.selectable:
                continue

            if item in checked_items:
                item.selected = True
                return item

            checked_items.add(item)

            if not found:
                if item.selected:
                    found = True
                    item.selected = False
                    continue
            else:
                item.selected = True
                return item

    def select_previous(self):
        """select the prev valid item, looping"""
        return self.select_next(prev=True)

    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            key = event.key
            if key in (pygame.K_DOWN, pygame.K_s):
                self.select_next()
            elif key in (pygame.K_UP, pygame.K_w):
                self.select_previous()
            elif key in (pygame.K_RETURN, pygame.K_KP_ENTER, pygame.K_SPACE):
                self.activate()
                print("Menu item activated: %s" % str(self.get_selected()))
            else:
                return False
        else:
            return False

        return True

