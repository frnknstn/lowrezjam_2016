from __future__ import division
from __future__ import print_function

import os
import collections
import random
import math

import pygame

from defines import *

class MissingFramesException(Exception):
    pass

image_cache = None

def load_sprite_files():
    """Load sprites from the disk which match the expected format.

    format is spritename_frameset_direction_index.png
    """
    filenames = os.listdir(SPRITE_DIR)
    filenames.sort()

    image_cache = collections.defaultdict(lambda: collections.defaultdict(dict))

    for filename in filenames:
        filepath = os.path.join(SPRITE_DIR, filename)
        if not os.path.isfile(filepath):
            continue

        # check the filename matches our expected format
        try:
            file_spritename, file_frameset_name, file_direction, file_index = os.path.splitext(filename)[0].split("_")
        except ValueError:
            debug("Ignoring misnamed file in sprite directory, '%s'" % filename)
            continue

        file_index = int(file_index)
        debug("Loading sprite '%s': '%s'-type's %s-facing frame %d of animation %s" % \
              (filename, file_spritename, file_direction, file_index, file_frameset_name))

        # load the sprite
        surf = pygame.image.load(filepath)
        surf.convert_alpha()

        # store in our cache
        key = (file_frameset_name, file_direction)
        image_cache[file_spritename][key][file_index] = surf


    # if there are no left frames for a right image, use a flipped right image
    for spritename, cache in image_cache.items():
        for key, frameset in cache.items():
            frameset_name, direction = key
            
            if direction != "right" or (frameset_name, "left") in cache:
                continue

            debug("Flipping %s's right-facing %s animation" % (spritename, frameset_name))
            flipped_frameset = {}
            for index, image in frameset.items():
                flipped_frameset[index] = pygame.transform.flip(image, True, False)

            cache[(frameset_name, "left")] = flipped_frameset

    # store the frames in the main cache
    for spritename in image_cache:
        Sprite.image_cache[spritename] = {}
        for key, value in image_cache[spritename].items():
            Sprite.image_cache[spritename][key] = [ value[x] for x in sorted(value) ]

class Sprite(pygame.sprite.Sprite):
    image_cache = {}

    def __init__(self, sprite_name, pos):
        """Create a new Sprite from the files named 'sprite-name_*.png' centered at pos

        :type sprite_name: str
        :type pos: tuple[float, float]
        """
        super(Sprite, self).__init__()

        # filename prefix used to load sprites
        self.sprite_name = sprite_name

        # remove this sprite after this frame
        self.dying = False

        # set up our animations
        self.animation = None  # our current animation coroutine
        self.image = None
        self.direction = "None"
        self.frame_sets = self.image_cache[sprite_name]

        # sample any of our sprites to determine our size
        if len(self.frame_sets) != 0:
            size = self.frame_sets.values()[0][0].get_size()
        else:
            size = (0, 0)

        # set up our positions
        self.pos = pos  # center of the object
        self.rect = pygame.Rect((0, 0), size)  # actual sprite's draw rect
        self.rect.center = pos

        # do collision detection?
        self.solid = False

        self.start_anim(self.anim_idle())

    def update(self, dt):
        if not self.dying:
            self.update_animation(dt)
            self.update_state(dt)
        else:
            self.kill()

    def update_state(self, dt):
        """update our internal status, called once per frame

        This method is intended to be overridden as needed by derived classes
        """
        pass

    def update_animation(self, dt):
        """update our animation"""
        if self.animation is None:
            return False

        try:
            retval = self.animation.send(dt)
        except StopIteration:
            debug("Animation on %s ran out!" % repr(self))
            self.animation = None
            retval = False

        return retval

    def get_frames(self, animation_name):
        """Get the most suitable set of frames for the given animation"""
        direction = self.direction

        # find a direction we have images for
        direction_found = True
        if (animation_name, direction) not in self.frame_sets:
            direction_found = False
            for priority in ("none", "right", "up", "down", "left"):
                if (animation_name, priority) in self.frame_sets:
                    direction = priority
                    direction_found = True
                    break

        # default to get the idle frames if we can't find any here
        if not direction_found:
            if animation_name != "idle":
                try:
                    retval = self.get_frames("idle")
                except MissingFramesException:
                    # raise exception below
                    pass
                else:
                    return retval
            raise MissingFramesException("Can't find any '%s' frames for %s" % (animation_name, str(self)))

        return self.frame_sets[(animation_name, direction)]

    def start_anim(self, anim):
        """Start playing an animation

        :type anim: generator
        """
        self.animation = anim
        return anim.next()

    def anim_normal(self, anim_name, frame_delay=0.1):
        """Animation coroutine for a standard animation loop"""
        frames = self.get_frames(anim_name)
        frame_time = 0

        while True:
            for frame in frames:
                self.image = frame
                new_frame = True

                while True:
                    dt = yield new_frame
                    frame_time += dt
                    if frame_time > frame_delay:
                        frame_time -= frame_delay
                        break

    def anim_once(self, anim_name, frame_delay=0.1, next_anim=None, reverse_order=False):
        """play an animation once and then move on to a different one"""
        frames = self.get_frames(anim_name)
        frame_time = 0

        if reverse_order:
            frames = reversed(frames)

        for frame in frames:
            self.image = frame
            new_frame = True

            while True:
                dt = yield new_frame
                frame_time += dt
                if frame_time > frame_delay:
                    frame_time -= frame_delay
                    break

        if next_anim is None:
            self.dying = True
        else:
            yield self.start_anim(next_anim())

    def anim_idle(self):
        return self.anim_normal("idle")

    def __repr__(self):
        if self.animation is None:
            animation = "None"
        else:
            animation = self.animation.__name__
        return "%s(%s, 0x%X)" % (super(Sprite, self).__repr__(), animation, id(self))


class Mob(Sprite):
    def __init__(self, sprite_name, pos, direction="none"):
        """Base class for all mobile sprites

        :type sprite_name: str
        :type pos: tuple[float, float]
        :type direction: str
        """
        super(Mob, self).__init__(sprite_name, pos)

        self.dx = self.dy = 0.0  # X and Y velocity in pixels / second
        self.dr = self.theta = 0.0  # velocity vector in pixels / second

        self.mu_coefficient = 1.0  # friction multiplier for the object

        self.direction = None
        self.set_direction(direction)  # the facing of this sprite

    def set_direction(self, direction):
        assert direction in DIRECTIONS

        self.direction = direction
        self.start_anim(self.anim_idle())

    def set_move_vector(self, theta, dr):
        """change the mob's velocity vector, as polar coords in pixels / frame"""
        self.theta = theta
        self.dr = dr

        self.dx = dr * math.cos(theta)
        self.dy = (dr * math.sin(theta))

    def set_move_delta(self, dx, dy):
        """change the mob's velocity, as cartesian coords in pixels / frame"""
        self.dx = dx
        self.dy = dy

        self.theta = math.atan2(dy, dx)
        self.dr = math.hypot(dx, dy)

    def move(self, dt):
        """Move us one frame's worth

        :type dt: float
        """
        if self.dx == 0 and self.dy == 0:
            return

        old_x, old_y = self.pos
        x, y = old_x + (self.dx * dt), old_y + (self.dy * dt)

        # do the move
        self.move_to(x, y)

    def move_to(self, x, y):
        """Move us to the specified coordinates"""
        self.pos = (x, y)
        self.rect.center = (round(x), round(y))

    def anim_lerp_to(self, target_pos, time, anim_name, frame_delay=0.1, next_anim=None):
        """Animation coroutine to lerp this sprite to another object

        If a next_anim is supplied, change to that when the lerp is over, otherwise kill the sprite.

        :type target_pos: tuple[float, float]
        :type time: float
        :type anim_name: str
        :type frame_delay: float
        :type next_anim: generator
        """

        frame_generator = self.anim_normal(anim_name, frame_delay)
        total_dt = 0
        start_x, start_y = self.pos

        dt = yield frame_generator.next()

        while total_dt < time:
            total_dt += dt
            total_dt = min(total_dt, time)
            distance_factor = total_dt / time

            # slerp
            # distance_factor = math.sin(distance_factor * math.pi / 2)
            #
            # smootherstep
            # t = distance_factor
            # distance_factor = t * t * t * (t * (6 * t - 15) + 10)

            x = start_x + (distance_factor * (target_pos[0] - start_x))
            y = start_y + (distance_factor * (target_pos[1] - start_y))
            self.move_to(x, y)

            dt = yield frame_generator.send(dt)

        # we are done, one last frame
        self.move_to(*target_pos)

        if next_anim is None:
            print("lerp over, dying")
            self.dying = True
            yield frame_generator.send(dt)
        else:
            print("lerp over, starting " + str(next_anim))
            yield self.start_anim(next_anim)

    def anim_hide(self):
        """Move this sprite offscreen"""
        self.move_to(-10, -10)
        yield self.start_anim(self.anim_normal("idle"))

class WaveSprite(Mob):
    def __init__(self, sprite_name, pos, direction_scale, wave_period=1):
        """Cool wave-back-and-forth visual effect sprite.

        The speed of the waving is controlled by wave_period. The severity on the X and Y
        axes is controlled by the direction_scale tuple.

        :type sprite_name: str
        :type pos: tuple[float, float]
        :type direction_scale: tuple[float, float]
        :type time_scale: float
        """
        super(WaveSprite, self).__init__(sprite_name, pos)

        self.start_anim(self.anim_wave(direction_scale, wave_period))

    def anim_wave(self, direction_scale, wave_period):
        """Animation co-routine for idle animation"""
        frames = self.get_frames("idle")
        wave_time = 0.0
        frame_delay = 3.0
        wave_freq_scale = (1 / wave_period) * math.pi * 2
        new_frame = True

        offset_x, offset_y = 0, 0
        x, y = self.pos

        while True:
            for frame in frames:
                self.image = frame
                new_frame = True
                next_frame_time = wave_time + frame_delay

                while wave_time < next_frame_time:
                    # calculate our new position
                    x, y = self.pos[0] - offset_x, self.pos[1] - offset_y
                    offset_x = (direction_scale[0] * math.sin(wave_time * wave_freq_scale))
                    offset_y = (direction_scale[1] * math.cos(wave_time * wave_freq_scale))
                    x, y = x + offset_x, y + offset_y
                    self.move_to(x, y)

                    dt = yield new_frame
                    wave_time += dt
                    new_frame = False

            dt = yield False

class RawSprite(pygame.sprite.Sprite):
    def __init__(self, image, pos):
        """
        Lower-level wrapper class for a sprite that avoids the sprite cache and anim system

        :type image: pygame.Surface
        :type rect: pygame.Rect
        """

        super(RawSprite, self).__init__()

        self.image = image

        # set up our positions
        self.pos = pos  # center of the object
        self.rect = pygame.Rect((0, 0), image.get_size())  # actual sprite's draw rect
        self.rect.center = pos

        self.dying = False

    def update(self, dt):
        if not self.dying:
            self.update_state(dt)
        else:
            self.kill()

    def update_state(self, dt):
        """update our internal status, called once per frame

        This method is intended to be overridden as needed by derived classes
        """
        pass
