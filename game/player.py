from __future__ import division
from __future__ import print_function

import random

import pygame

from defines import *
import sprite_types
import decks


class WizardSprite(sprite_types.Sprite):
    def __init__(self, sprite_name, pos,):
        super(WizardSprite, self).__init__(sprite_name, pos)

        self.start_anim(self.anim_normal("talk", 0.1))


class Player(object):
    def __init__(self, name, side, render_group, human=True, local=True, deck=None):
        """A player in the game

        :type name: str
        :type side: "home" or "away"
        :type render_group: pygame.sprite.LayeredUpdates
        :type human: bool
        :type local: bool
        :type deck: dict[str, int]
        """

        self.name = name
        self.side = side

        self.actions = []   # type: list[Action]
        self.busy = False

        self.human = human
        self.local = local

        # spells
        self.active_spell = None    # type: Spell
        self.storage = []
        self.cast_reversed = False

        if deck is not None:
            # TODO: implement this :)
            pass
        else:
            # use a default deck
            deck = decks.basic

        self.cloud = AetherCloud(deck_contents=deck)

        # our sprite
        if side == "home":
            wizard_pos = WIZARD_HOME_POS
            wizard_direction = "right"
        else:
            wizard_pos = WIZARD_AWAY_POS
            wizard_direction = "left"
        wizard = WizardSprite("zipwizard", wizard_pos)
        wizard.direction = wizard_direction
        wizard.start_anim(wizard.anim_normal("talk", 0.2))
        render_group.add(wizard, layer=LAYER_WIZARD)
        self.render_group = render_group
        self.wizard = wizard

    def add_to_render_group(self, spr, layer, render_group=None):
        """Add a sprite to a/our render group

        :type spr: any
        :type layer: int
        :type render_group: optional[pygame.sprite.LayeredUpdates]
        """
        if render_group is None:
            render_group = self.render_group
        render_group.add(spr, layer=layer)

    def add_cloud_to_render_group(self):
        """Add all the spells in our cloud to a render group"""
        render_group = self.render_group
        count = 0

        for spell in self.cloud.deck:
            self.add_to_render_group(spell.aether, layer=LAYER_SPELLS)
            count += 1

        print("Added %d aether to %s" % (count, str(render_group)))

    def handle_event(self, event):
        """Event handler for player input"""
        if event.type == pygame.KEYDOWN:
            key = event.key
            # handle the reversed cast state
            if not self.cast_reversed:
                cast_action_type = "cast"
            else:
                cast_action_type = "cast reversed"

            if key == pygame.K_q:
                self.add_action(self, cast_action_type, 0)
            elif key == pygame.K_w:
                self.add_action(self, cast_action_type, 1)
            elif key == pygame.K_e:
                self.add_action(self, cast_action_type, 2)
            elif key in (pygame.K_LSHIFT, pygame.K_RSHIFT):
                self.cast_reversed = True
            elif event.key in (pygame.K_SPACE,):
                if not self.cast_reversed:
                    self.add_action(self, "draw from aether")
                else:
                    self.add_action(self, "draw from staff")
            else:
                return False
        elif event.type == pygame.KEYUP:
            if event.key in (pygame.K_LSHIFT, pygame.K_RSHIFT):
                self.cast_reversed = False
            else:
                return False
        else:
            return False

        # we handled it!
        return True

    def handle_event_away(self, event):
        """Event handler for player input

        THIS IS TEMP CODE
        if we are the away player, use a different set of rules
        """
        if event.type == pygame.KEYDOWN:
            # handle the reversed cast state
            if not self.cast_reversed:
                cast_action_type = "cast"
            else:
                cast_action_type = "cast reversed"

            if event.key == pygame.K_y:
                self.add_action(self, cast_action_type, 0)
            elif event.key == pygame.K_u:
                self.add_action(self, cast_action_type, 1)
            elif event.key == pygame.K_i:
                self.add_action(self, cast_action_type, 2)
            else:
                return False
        elif event.type == pygame.KEYUP:
            if False:
                pass
            else:
                return False
        else:
            return False

        # we handled it!
        return True

    def add_action(self, *args, **kwargs):
        """add an action to our action queue"""
        self.actions.append(Action(*args, **kwargs))

    def draw_from_aether(self):
        """
        Get the next spell from our cloud to be the active spell

        :rtype: Spell or None
        """
        if len(self.cloud) == 0:
            print("Not drawing from an empty cloud")
            return None

        if self.active_spell:
            self.storage.append(self.active_spell)
        self.active_spell = self.cloud.pop()
        print ("Drew %s from aether" % str(self.active_spell))

        return self.active_spell

    def draw_from_staff(self):
        """

        :rtype: Spell or None
        """
        if not self.storage:
            print("unable to draw from empty storage")
            return None

        stored_spell = self.storage.pop()

        if self.active_spell:
            self.storage.append(self.active_spell)
        self.active_spell = stored_spell
        print ("Drew %s from storage" % str(self.active_spell))

        return self.active_spell


class Spell(object):
    def __init__(self, spell_type):
        """A spell

        :type spell_type: str
        """
        assert spell_type in SPELLS
        self.spell_type = spell_type
        self.color = SPELLS[spell_type]

        # make us a visible aether spark
        randfloat = random.random
        sprite_name = "%s-aether" % self.color

        wave_period, direction_scale = randfloat() * 2 + 3, (randfloat() * 2 + 2, randfloat() * 2 + 2)
        self.aether = sprite_types.WaveSprite(sprite_name, CLOUD_POS_HOME, direction_scale, wave_period)

    def hide(self):
        """Move this spell offscreen"""
        self.aether.start_anim(self.aether.anim_hide())
        
    def __str__(self):
        return '<Spell("%s") id 0x%xd>' % (self.spell_type, id(self))


class AetherCloud(object):
    def __init__(self, deck_contents):
        """A pretty cloud of aether, circling above a wizard"""
        self.deck = []
        deck = self.deck

        for spell_type in deck_contents:
            deck.append(Spell(spell_type))

        random.shuffle(deck)

        # add the 3 seal spells
        self.deck = [Spell("seal"), Spell("seal"), Spell("seal")] + deck

    def pop(self):
        """Draw a spell from the aether cloud

        :rtype: Spell or None
        """
        try:
            retval = self.deck.pop()
        except IndexError:
            print("Aether cloud is now empty")
            retval = None

        return retval

    def __len__(self):
        return len(self.deck)


class Action(object):
    def __init__(self, player, action_type, target=None):
        """A player action

        :type player: game.player.Player
        :type action_type: str
        :type target: any
        :type spell_type: optional[str]
        """
        self.player_side = player.side
        assert (action_type in ACTION_TYPES)
        self.action_type = action_type
        self.target = target

    def __repr__(self):
        return "<Action %s player %s at %s>" % (
            self.player_side,
            self.action_type,
            str(self.target)
        )





