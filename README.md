## Blast #

Entry for lowrezjam 2016

Inspired by real-time card games like "Brawl" and wizard games like "Sacrifice" and
"Magic Carpet".

Needed art:

* Battlefield
  * two wizards
  * islands
* castles
  * rotation animation
* spell fx
* damage markers
* font?
* winner animations?

Abandoned ideas:

* three towns / islands
  * dividing wall
  * owner flag?
  * rotation animation

Ability ideas:

* fireball - red damage
* magic missile - magenta damage
* lightning bolt - cyan damage
* tsunami - remove edge island
* rotate - swap settlements on an island
* shields - red, magenta and cyan shields
* force wall - ???
* dispel - ???
* settle - put a castle on an island
* develop - improve a castle
* seal - Stasis?
* meteor shower / raze
* spellsteal / mana leak
* volcano - create a new island?
* earthquake

Multiplayer messages:

New plan: just send events / actions

Read input:
game.parse_events() -> player.handle_event() -> player.actions

Write to server:
game.send_player_actions() -> \/\cloud/\/

Read from server:
game.recv_actions() -> game.handle_actions()

## Todo #

* sound
* formal menu
* implement dispel
* implement tutorial
* special effects
  * cast lines
  * element hint in spell name line
  * smooth move of spell name to secondary spell name
* music
* remote multiplayer support
* wizard choice
* settings?

## Old Todo #

* Short term:
  * draw from aether
  * store in staff
* Mid term:
  * basic spells
    * settle
    * improve
    * firebolt
  * intermediate spells
    * tsunami
    * volcano
  * 
   


## How to run from source #

 1. Install Python 2.7 from here:
    https://www.python.org/downloads/release/python-279/
 2. Install PyGame 1.9.2. If you are on Windows, you can find a recent-ish build here:
    https://bitbucket.org/pygame/pygame/downloads/pygame-1.9.2a0.win32-py2.7.msi
 3. Double-click 'game.py'

Also feel free to check out the git repo on Bitbucket:
https://bitbucket.org/frnknstn/lowrezjam_2016


## Credits #

See also reference_art/licences.txt

* Wizard sprite
  * By Monster Logix Studio
  * From the game ZipWizard
  * https://opengameart.org/content/mega-pixel-art-sheet
  * Licence: CC0
* GNU FreeSansBold font
  * Part of the GNU FreeFont family
  * The default PyGame font
  * https://www.gnu.org/software/freefont/
  * Licence: GNU GPLv3 with font exception

