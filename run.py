#!/usr/bin/python

"""
lowrezjam 2016 entry
"""

from __future__ import division
from __future__ import print_function

import game
game.main(game.BlastGame)
